import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.lang.Object;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;

import org.basex.BaseXServer;
import org.basex.api.client.ClientSession;
import org.basex.core.cmd.XQuery;
import generated.*;
import org.inria.fr.ns.cr.Crs;
import org.inria.fr.ns.cr.Crs.Cr;
import org.inria.fr.ns.sr.*;
import org.xml.sax.SAXException;

@Path("/")
public class RawebServer {
	
    BaseXServer server;
    ClientSession session;

    private StructureInrias ListeDesProjets;
    private Crs listCentreRecherche;
    private ArrayList<generated.Raweb> rapportDActiviteDeProjet;
   

    public RawebServer() throws IOException, ParseException, JAXBException, SAXException, ParserConfigurationException{
    	
    	if(!BaseXServer.ping("localhost", 1984)) {
	    	server = new BaseXServer();
	    	session = new ClientSession("localhost", 1984, "admin", "admin");
    	}else {
	    	session = new ClientSession("localhost", 1984, "admin", "admin");
    	}
    	session = RawebUtil.VerifySessionDB(session);
    	session = RawebUtil.VerifySessionCache(session);
    	
    	//System.out.println(session.execute(new XQuery("db:info('LocalDB')")));
    	
        this.ListeDesProjets = this.getListDesProjets();
        this.listCentreRecherche = this.getAllListCentreRecherche();
        //System.out.println(session.execute(new XQuery("db:info('LocalDB')")));
        this.rapportDActiviteDeProjet=this.getAllProject();
        //System.out.println("Tout les projets : "+allProject.size());
    	
    	//session.execute("DROP DB LocalDB");
    	//session.close();
    	//server.stop();
    }

    /**
     * On récupère toutes les Team disponible dans le fichier xml : bastri
     * @return toutes les Team disponible
     * @throws JAXBException 
     * @throws IOException 
     */
    private StructureInrias getListDesProjets() throws JAXBException, IOException{
    	
    	// instantiation du unmarshaller pour StructureInrias
        JAXBContext jaxb = JAXBContext.newInstance(StructureInrias.class);
        Unmarshaller unmarshaller = jaxb.createUnmarshaller();
        
        // reader pour bastri.xml retourné par la base
        StringReader reader = new StringReader(session.execute(new XQuery("db:open('LocalDB', 'bastri.xml')")));
        
        // TODO verification par XSD
        return (StructureInrias) unmarshaller.unmarshal(reader);
    }

    /**
     * On récupère toutes les zones géographics qui se trouve dans le fichier xml :bastriCris.xml
     * @return toutes les zones géographiques disponible
     * @throws JAXBException 
     * @throws IOException 
     */
    private Crs getAllListCentreRecherche() throws JAXBException, IOException{
    	
    	// instantiation du unmarshaller pour Crs
        JAXBContext jaxb = JAXBContext.newInstance(Crs.class);
        Unmarshaller unmarshaller = jaxb.createUnmarshaller();
        
        // reader pour bastriCris.xml retourné par la base
        StringReader reader = new StringReader(session.execute(new XQuery("db:open('LocalDB', 'bastriCris.xml')")));
        
        // TODO verification par XSD
        return (Crs) unmarshaller.unmarshal(reader);
    }

    /**
     * On récupère tous les projets disponibles dans les fichiers XMl se trouvant dans la base
     * @return tous les projets disponibles
     */
    private ArrayList<generated.Raweb> getAllProject() {
        //TODO changer la récupération des fichier direct en base de donnée avec du XQUery
        ArrayList<generated.Raweb> allProject = new ArrayList<generated.Raweb>();
        File repertoire = new File("./src/main/webapp/WEB-INF/allXmlFile/Raweb/");
        File[] listefichiers;
        int i;

        listefichiers = repertoire.listFiles();
        for (i = 0; i < listefichiers.length; i++) {
            if (listefichiers[i].getAbsolutePath().endsWith(".xml") == true) {

                try {
                    JAXBContext jaxb = JAXBContext.newInstance(Raweb.class);
                    Unmarshaller unmarshaller = jaxb.createUnmarshaller();
                    allProject.add( (generated.Raweb) unmarshaller.unmarshal(listefichiers[i]));
                }catch (JAXBException e) {
                    e.printStackTrace();
                }
            }
        }
        return allProject;
    }

    /**
     * lister les centres de recherche, les présenter sur une carte (GoogleMap, OpenStreetMap, etc),
     * le nombre d'équipe dans chaque centre, le nombre de personnes par centre, etc.
     *  Distinguer les équipes fermées/actives (<date_fermeture>)
     * @return la liste des centres de recherches sous formes de JSON
     * @throws IOException
     */
    @GET
    @Path("/listResearchCenter")
    @Produces("application/json")
    public Response listResearchCenter() throws IOException, ParseException {
    	
        String response = null;
        if(this.listCentreRecherche!=null && this.ListeDesProjets!=null && this.rapportDActiviteDeProjet!=null){
            response = "[";
            List<Crs.Cr> Crs = this.listCentreRecherche.getCr();
            for(int i=0;i<Crs.size();i++){
                Cr cr =Crs.get(i);
                response+="{";
                Cr.Adressegeographique adr = cr.getAdressegeographique();
                response+="\"Adressegeographique\":{\"adresse\":\""+ adr.getAdresse()+"\",\"codePostal\":\""+adr.getCp()+"\",\"lattitude\":\""+adr.getLatitude()+"\",\"longitude\":\""+adr.getLongitude()+"\",\"ville\":\""+adr.getVille()+"\",\"libelle\":\""+adr.getLibelle()+"\"},";
                response+="\"libelle\":\""+cr.getLibelle()+"\",\"nbResponsable\":\""+cr.getResponsable().size()+"\",";
                response+="\"nbEquipeActive\":\""+this.getNbEquipeForOneCenterActive(adr.getSiid())+"\",";
                response+="\"nbEquipeInActive\":\""+this.getNbEquipeForOneCenterInactive(adr.getSiid())+"\",";
                response+="\"nbPersonne\":\""+this.getNbPersonneForOneCenter(adr.getCri().getValue())+"\"";
                if(i<Crs.size()-1){
                    response+="},";
                }else{
                    response+="}";
                }
            }
            response+="]";
        }
        return Response
        		.status(200)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                .header("Access-Control-Max-Age", "1209600")
                .entity(response)
                .build();
    }

    /**
     * lister les équipes par centre de recherche et par domaines/thèmes de recherche, le nombre de personnes sur ces critères, etc
     * @return la liste des équipes en JSON
     */
    @GET
    @Path("/listTeam")
    @Produces("application/json")
    public Response listTeam(){
        String response = "[";
        if(this.listCentreRecherche!=null && this.ListeDesProjets!=null && this.rapportDActiviteDeProjet!=null){

            List<Crs.Cr> Crs = this.listCentreRecherche.getCr();
            for(int i=0;i<Crs.size();i++){
                response += "{";
                Cr cr = Crs.get(i);
                response+= "\"libelle\":\""+cr.getLibelle()+"\",";
                response+="\"nbPersonne\":\""+this.getNbPersonneForOneCenter(cr.getAdressegeographique().getCri().getValue())+"\",";
                response+= "\"themes\":[";
                ArrayList<Structureinria> allTeamCenter = this.getAllTeamCenter(cr.getAdressegeographique().getCri().getValue());
                ArrayList<Theme> allTheme = this.getAllTheme(allTeamCenter);
                for(int j=0;j<allTheme.size();j++){
                    Theme theme = allTheme.get(j);
                    response+="{\"nom\":\""+theme.getValue()+"\",";

                    //response+="\""+theme.getValue()+"\":{";
                    response+="\"equipes\":["+this.getStringTeamForTheme(allTeamCenter,theme)+"]";
                    if(j<allTheme.size()-1){
                        response+="},";
                    }else{
                        response+="}]";
                    }
                }
                if(i<Crs.size()-1){
                    response+="},";
                }else{
                    response+="}";
                }
            }
            //response += "}";
        }
        response += "]";
        return Response
                .status(200)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                .header("Access-Control-Max-Age", "1209600")
                .entity(response)
                .build();
    }

    /**
     * lister les personnels des équipes, la liste des titres du rapport auxquels participe une personne (<participants>), le nombre de ses publications (<biblStruct>)
     * @return la liste des personnels en JSON
     */
    @GET
    @Path("/listPersonnelsTeam")
    @Produces("application/json")
    public Response listPersonnelsTeam(){
        String response = "[";
        if(this.ListeDesProjets!=null && this.rapportDActiviteDeProjet!=null) {
            for(int i=0;i<this.ListeDesProjets.getStructureinria().size();i++){
                Structureinria team = this.ListeDesProjets.getStructureinria().get(i);
                response+="{";
                for(UrlTeam url : team.getUrlTeam()){
                    response+= "\"urlTeam-"+url.getLang()+"\":\""+url.getValue()+"\",";
                }
                //TODO MARCHE PAS POUR LA VALIDATION JSON
                /*for(Resume resume : team.getResume()){
                    String rs =resume.getValue();
                    rs = rs.replaceAll("[\r\n]+", " ");
                    rs=rs.replaceAll("\"","'");

                    //response+= "\"resume-"+resume.getLang()+"\":\""+rs+"\",";
                }*/
                for(Theme t :team.getTheme()){
                    response+= "\"theme-"+t.getLang()+"\":\""+t.getValue()+"\",";
                }
                for(Domaine d :team.getDomaine()){
                    response+= "\"domaine-"+d.getLang()+"\":\""+d.getValue()+"\",";
                }
                response+= "\"entite\":[";
                for(int j=0;j<team.getEntite().size();j++){
                    Entite en = team.getEntite().get(j);
                    response+= "{";
                    response+="\"commentaire\":\""+en.getCommentaire()+"\",";
                    response+="\"principal\":\""+en.getPrincipal()+"\",";
                    response+="\"adresseGeographique\":{";
                    Adressegeographique adr = en.getAdressegeographique();
                    response+="\"latitude\":\""+adr.getLatitude()+"\",";
                    response+="\"longitude\":\""+adr.getLongitude()+"\",";
                    response+="\"adresse\":\""+adr.getAdresse()+"\",";
                    response+="\"cp\":\""+adr.getCp()+"\",";
                    response+="\"Ville\":\""+adr.getVille()+"\"";
                    response+="}";
                    if(j<team.getEntite().size()-1){
                        response+= "},";
                    }else{
                        response+= "}";
                    }
                }
                response+="],";
                List<Person> allPerson = this.getPersonForThisTeam(team);
                response+="\"personnes\":[";
                for(int j=0;j<allPerson.size();j++){
                    Person p = allPerson.get(j);
                    response+="{";
                    response+="\"firstname\":\""+p.getFirstname()+"\",";
                    response+="\"lastname\":\""+p.getLastname()+"\",";
                    response+="\"Hdr\":\""+p.getHdr()+"\",";
                    response+="\"Key\":\""+p.getKey()+"\",";
                    response+="\"ResearchCentre\":\""+p.getResearchCentre()+"\",";
                    if(p.getMoreinfo()!=null){
                        response+="\"CategoryPro\":\""+p.getCategoryPro()+"\",";
                        response+="\"Moreinfo\":\""+p.getMoreinfo().toString()+"\"";
                    }else{
                        response+="\"CategoryPro\":\""+p.getCategoryPro()+"\"";
                    }
                    ArrayList<Raweb> rawebs = new ArrayList<>();
                    for(Raweb r : this.getAllProject()){
                        ArrayList<Participants> participants = this.getAllParticipants(r);
                        for(Participants participants1: participants){
                            for(Person person : participants1.getPerson()){
                                if(person.getFirstname().equals(p.getFirstname()) && person.getLastname().equals(p.getLastname())){
                                    rawebs.add(r);
                                }
                            }
                        }
                    }
                    response+="\"Projects_participe\":[";
                    for(int y=0;y<rawebs.size();y++){
                        Raweb r = rawebs.get(y);
                        response+="{";
                        response+="\"porjectName\":\""+r.getIdentification().getProjectName()+"\",";
                        List<BiblStruct> bibli = r.getBiblio().getBiblStruct();
                        int nbPubliciation=0;
                        for(BiblStruct biblStruct:bibli){
                            if(biblStruct.getAnalytic()!=null && biblStruct.getAnalytic().getAuthor()!=null ){
                                List<PersName> persNames = biblStruct.getAnalytic().getAuthor().getPersName();
                                for(PersName persName : persNames){
                                    if(persName.getSurname().equals(p.getLastname())){
                                        nbPubliciation++;
                                    }
                                }
                            }
                        }
                        response+="\"nbPubliciation\":\""+nbPubliciation+"\"";
                        if(y<rawebs.size()-1){
                            response+= "},";
                        }else{
                            response+= "}";
                        }
                    }
                    response+="]";
                    //p.getMoreinfo();
                    if(j<allPerson.size()-1){
                        response+= "},";
                    }else{
                        response+= "}";
                    }
                }
                response+="],";
                response+="\"Test\":\"test\"";
                if(i<this.ListeDesProjets.getStructureinria().size()-1){
                    response+="},";
                }else{
                    response+="}";
                }
            }

        }
        response += "]";
        System.out.println(response);
        return Response
                .status(200)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                .header("Access-Control-Max-Age", "1209600")
                .entity(response)
                .build();
    }

    /**
     * Récupère tous les participants pour un rapport
     * @param r
     * @return
     */
    private ArrayList<Participants> getAllParticipants(Raweb r) {
        ArrayList<Participants> participants = new ArrayList<>();
        if(r.getResultats()!=null){
            List<Subsection> subsections = r.getResultats().getSubsection();
            for(Subsection subsection:subsections) {
                List<Object> allObjects = subsection.getBodyTitleOrMoreinfoOrP();
                for (Object o : allObjects) {
                    if (o.getClass().getName().equals("generated.Subsection")) {
                        Subsection c = (Subsection) o;
                        for (Object o1 : c.getBodyTitleOrMoreinfoOrP()) {
                            if (o1.getClass().getName().equals("generated.Participants")) {
                                Participants p = (Participants) o1;
                                participants.add(p);
                            }
                        }
                    }
                }
            }
        }
        return participants;
    }

    /**
     * Récupère la liste de personne contenu dans une team
     * @param team
     * @return la liste de personne contenu dans une team
     */
    private List<Person> getPersonForThisTeam(Structureinria team) {
        List<Person> persons = new ArrayList<>();
        for(UrlTeam url : team.getUrlTeam()) {
            for (generated.Raweb raweb : this.rapportDActiviteDeProjet) {
                if (raweb.getIdentification().getUrlTeam() != null && raweb.getIdentification().getUrlTeam().equals(url.getValue())) {
                    Team m = raweb.getTeam();
                    persons=m.getPerson();
                    break;
                }
            }
        }
        return persons;
    }

    /**
     * Verifie si une équipe est déjà comprise dans une list d'équipe
     * @param allteam
     * @param SiidEquipe
     * @return
     */
    private boolean existInArrayList(ArrayList<Structureinria> allteam,String SiidEquipe){
        boolean t = false;
        for(Structureinria s : allteam){
            if(s.getSiidEquipeGroupe().equals(SiidEquipe)){
                t=true;
                break;
            }
        }
        return t;
    }

    /**
     * Récupère un JSON pour toutes les équipes dans un thème particulier
     * @param allTeamCenter
     * @param theme
     * @return
     */
    private String getStringTeamForTheme(ArrayList<Structureinria> allTeamCenter, Theme theme) {
        String response = "";
        ArrayList<Structureinria> teamOfTheme= new ArrayList<>();
        for(Structureinria sri:allTeamCenter){
            for(Theme themeTeam : sri.getTheme()){
                if(themeTeam.getLang().equals("fr") && themeTeam.getValue().equals(theme.getValue())){
                    if(!this.existInArrayList(teamOfTheme,sri.getSiidEquipeGroupe())){
                        teamOfTheme.add(sri);
                        break;
                    }
                }
            }
        }
        for(int i =0;i<teamOfTheme.size();i++){
            Structureinria team = teamOfTheme.get(i);
            response+="{\"id\":\""+team.getSiidEquipeGroupe()+"\",";
            response+="\"libelle\":\""+team.getLibellefr()+"\",";
            response+="\"idhal\":\""+team.getIdhal()+"\",";
            response+="\"dateCreation\":\""+team.getDateCreation()+"\",";
            response+="\"dateFermeture\":\""+team.getDateFermeture()+"\",";
            response+="\"numNatStruct\":\""+team.getNumnatstruct()+"\",";
            response+="\"typesStructure\":\""+team.getTypestructure()+"\",";

            for(UrlTeam url : team.getUrlTeam()){
                if(url.getLang().equals("fr")){
                    response+="\"urlTeam\":\""+url.getValue()+"\",";
                }
            }

            for(Domaine m : team.getDomaine()){
                if(m.getLang().equals("fr")){
                    response+="\"Domaine\":\""+m.getValue()+"\",";
                }
            }
            for(Keywords k : team.getKeywords()){
                if(k.getLang().equals("fr")){
                    response+="\"Keywords\":\""+k.getValue()+"\",";
                }
            }
            for(int j=0;j<team.getEntite().size();j++) {
                Entite e = team.getEntite().get(j);
                response+="\"Entite-"+e.getSiid()+"\":{";
                response+="\"hebergeur\":\""+e.getHebergeur()+"\",";
                response+="\"commentaire\":\""+e.getCommentaire()+"\",";
                response+="\"codeStructure\":\""+e.getCodestructure()+"\",";
                response+="\"principal\":\""+e.getPrincipal()+"\",";
                response+="\"personne\":\""+e.getPersonne().getNom()+"-"+e.getPersonne().getPrenom()+"\",";
                response+="\"adresseGeographique\":{";
                Adressegeographique adr = e.getAdressegeographique();
                response+="\"cri\":\""+adr.getCri()+"\",";
                response+="\"adresse\":\""+adr.getAdresse()+"\",";
                response+="\"cp\":\""+adr.getCp()+"\",";
                response+="\"ville\":\""+adr.getVille()+"\",";
                response+="\"url\":\""+adr.getUrl()+"\",";
                response+="\"libelle\":\""+adr.getLibelle()+"\",";
                response+="\"longitude\":\""+adr.getLongitude()+"\",";
                response+="\"latitude\":\""+adr.getLatitude()+"\"";
                response+="}";
                response+="},";
            }
            response+="\"nbPersonne\":\""+this.getNbPersonne(team)+"\"";
            if(i<teamOfTheme.size()-1){
                response+="},";
            }else{
                response+="}";
            }
        }
        return response;
    }

    /**
     * Récupère le nombre de personne selon une équipe
     * @param team l'équipe dont on veut le nombre de membre
     * @return le nombre de personne dans une équipe
     */
    private int getNbPersonne(Structureinria team) {
        int nb =0;
        for(UrlTeam url : team.getUrlTeam()) {
            for (generated.Raweb raweb : this.rapportDActiviteDeProjet) {
                if (raweb.getIdentification().getUrlTeam() != null && raweb.getIdentification().getUrlTeam().equals(url.getValue())) {
                    nb = raweb.getTeam().getPerson().size();
                    break;
                }
            }
        }
        return nb;
    }

    /**
     * Récupère tous les theme lier à une arrayList d'équipe
     * @param allTeamCenter List des équipes contenant les themes que l'on veut
     * @return une list de tous les thèmes
     */
    private ArrayList<Theme> getAllTheme(ArrayList<Structureinria> allTeamCenter) {
        ArrayList<Theme> allTheme = new ArrayList<Theme>();
        ArrayList<String> allNameTheme = new ArrayList<String>();
        for(Structureinria sri : allTeamCenter){
              for(Theme themeTeam : sri.getTheme()){
                  if(themeTeam.getLang().equals("fr") && !allNameTheme.contains(themeTeam.getValue())){
                      allNameTheme.add(themeTeam.getValue());
                      allTheme.add(themeTeam);
                      break;
                  }
              }
        }
        return allTheme;
    }

    /**
     * Récupère toutes les équipes qui se trouve dans le centre de recherches demandés
     * @param CriCenter Cri du centre de recherche demandé
     * @return la liste des équipes se trouvant dans le centre de recherche
     */
    private ArrayList<Structureinria> getAllTeamCenter(String CriCenter){
        ArrayList<Structureinria> allTeamCenter = new ArrayList<Structureinria>();
        for(Structureinria sri : this.ListeDesProjets.getStructureinria()){
            for(Entite entite : sri.getEntite()){
                if(entite.getAdressegeographique().getCri().getValue().equals(CriCenter) ){
                    allTeamCenter.add(sri);
                    break;
                }
            }
        }
        return allTeamCenter;
    }

    /**
     * Compte le nombre de personne dans un centre
     * @param criCenter le centre demandé
     * @return le nombre de personne du centre
     */
    private int getNbPersonneForOneCenter(String criCenter) {
        int nbPersonneForThisCenter = 0;
        ArrayList<Structureinria> allTeam = this.getAllTeamCenter(criCenter);
        for(Structureinria team:allTeam){
            nbPersonneForThisCenter+=this.getNbPersonne(team);
        }
        return nbPersonneForThisCenter;
    }

    /**
     * Compte le nombre d'équipe active selon un centre donnée
     * @param siidCenter SIID du centre demandé
     * @return le nombre d'équipe qui compose le centre
     */
    private int getNbEquipeForOneCenterActive(String siidCenter) throws ParseException {
        int nbEquipeForThisCenter = 0;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        for(Structureinria sri : this.ListeDesProjets.getStructureinria()){
            boolean add =false;
            for(Entite entite : sri.getEntite()){
                if(!add && entite.getAdressegeographique().getSiid().equals(siidCenter) ){
                    if(sri.getDateFermeture()==null || formatter.parse(sri.getDateFermeture()).compareTo(new Date())>0){
                        nbEquipeForThisCenter++;
                        add=true;
                    }
                }
            }
        }
        return nbEquipeForThisCenter;
    }

    /**
     * Compte le nombre d'équipe inactive selon un centre donnée
     * @param siidCenter SIID du centre demandé
     * @return le nombre d'équipe qui compose le centre
     */
    private int getNbEquipeForOneCenterInactive(String siidCenter) throws ParseException {
        int nbEquipeForThisCenter = 0;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        for(Structureinria sri : this.ListeDesProjets.getStructureinria()){
            boolean add =false;
            for(Entite entite : sri.getEntite()){
                if(!add && entite.getAdressegeographique().getSiid().equals(siidCenter) ){
                    if(sri.getDateFermeture()==null || formatter.parse(sri.getDateFermeture()).compareTo(new Date())<=0){
                        nbEquipeForThisCenter++;
                    }
                }
            }
        }
        return nbEquipeForThisCenter;
    }


    /**
     * Main de tests
     * @param args
     * @throws Exception
     */
    public static void main(final String[] args) throws Exception {
        RawebServer r =new RawebServer();
        r.listPersonnelsTeam();
    }
    
}
