//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2018.01.24 à 09:57:38 AM CET 
//


package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}idno" minOccurs="0"/>
 *         &lt;choice maxOccurs="unbounded">
 *           &lt;element ref="{}title"/>
 *           &lt;element ref="{}editor"/>
 *         &lt;/choice>
 *         &lt;choice minOccurs="0">
 *           &lt;element ref="{}author"/>
 *           &lt;element ref="{}loc"/>
 *         &lt;/choice>
 *         &lt;element ref="{}imprint"/>
 *         &lt;element ref="{}meeting" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}NCName" />
 *       &lt;attribute name="x-editorial-board" type="{http://www.w3.org/2001/XMLSchema}NCName" />
 *       &lt;attribute name="x-international-audience" type="{http://www.w3.org/2001/XMLSchema}NCName" />
 *       &lt;attribute name="x-invited-conference" type="{http://www.w3.org/2001/XMLSchema}NCName" />
 *       &lt;attribute name="x-proceedings" type="{http://www.w3.org/2001/XMLSchema}NCName" />
 *       &lt;attribute name="x-scientific-popularization" type="{http://www.w3.org/2001/XMLSchema}NCName" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idno",
    "titleOrEditor",
    "author",
    "loc",
    "imprint",
    "meeting"
})
@XmlRootElement(name = "monogr")
public class Monogr {

    protected Idno idno;
    @XmlElements({
        @XmlElement(name = "title", type = Title.class),
        @XmlElement(name = "editor", type = Editor.class)
    })
    protected List<java.lang.Object> titleOrEditor;
    protected Author author;
    protected String loc;
    @XmlElement(required = true)
    protected Imprint imprint;
    protected Meeting meeting;
    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String id;
    @XmlAttribute(name = "x-editorial-board")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String xEditorialBoard;
    @XmlAttribute(name = "x-international-audience")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String xInternationalAudience;
    @XmlAttribute(name = "x-invited-conference")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String xInvitedConference;
    @XmlAttribute(name = "x-proceedings")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String xProceedings;
    @XmlAttribute(name = "x-scientific-popularization")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String xScientificPopularization;

    /**
     * Obtient la valeur de la propriété idno.
     * 
     * @return
     *     possible object is
     *     {@link Idno }
     *     
     */
    public Idno getIdno() {
        return idno;
    }

    /**
     * Définit la valeur de la propriété idno.
     * 
     * @param value
     *     allowed object is
     *     {@link Idno }
     *     
     */
    public void setIdno(Idno value) {
        this.idno = value;
    }

    /**
     * Gets the value of the titleOrEditor property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the titleOrEditor property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTitleOrEditor().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Title }
     * {@link Editor }
     * 
     * 
     */
    public List<java.lang.Object> getTitleOrEditor() {
        if (titleOrEditor == null) {
            titleOrEditor = new ArrayList<java.lang.Object>();
        }
        return this.titleOrEditor;
    }

    /**
     * Obtient la valeur de la propriété author.
     * 
     * @return
     *     possible object is
     *     {@link Author }
     *     
     */
    public Author getAuthor() {
        return author;
    }

    /**
     * Définit la valeur de la propriété author.
     * 
     * @param value
     *     allowed object is
     *     {@link Author }
     *     
     */
    public void setAuthor(Author value) {
        this.author = value;
    }

    /**
     * Obtient la valeur de la propriété loc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoc() {
        return loc;
    }

    /**
     * Définit la valeur de la propriété loc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoc(String value) {
        this.loc = value;
    }

    /**
     * Obtient la valeur de la propriété imprint.
     * 
     * @return
     *     possible object is
     *     {@link Imprint }
     *     
     */
    public Imprint getImprint() {
        return imprint;
    }

    /**
     * Définit la valeur de la propriété imprint.
     * 
     * @param value
     *     allowed object is
     *     {@link Imprint }
     *     
     */
    public void setImprint(Imprint value) {
        this.imprint = value;
    }

    /**
     * Obtient la valeur de la propriété meeting.
     * 
     * @return
     *     possible object is
     *     {@link Meeting }
     *     
     */
    public Meeting getMeeting() {
        return meeting;
    }

    /**
     * Définit la valeur de la propriété meeting.
     * 
     * @param value
     *     allowed object is
     *     {@link Meeting }
     *     
     */
    public void setMeeting(Meeting value) {
        this.meeting = value;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété xEditorialBoard.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXEditorialBoard() {
        return xEditorialBoard;
    }

    /**
     * Définit la valeur de la propriété xEditorialBoard.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXEditorialBoard(String value) {
        this.xEditorialBoard = value;
    }

    /**
     * Obtient la valeur de la propriété xInternationalAudience.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXInternationalAudience() {
        return xInternationalAudience;
    }

    /**
     * Définit la valeur de la propriété xInternationalAudience.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXInternationalAudience(String value) {
        this.xInternationalAudience = value;
    }

    /**
     * Obtient la valeur de la propriété xInvitedConference.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXInvitedConference() {
        return xInvitedConference;
    }

    /**
     * Définit la valeur de la propriété xInvitedConference.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXInvitedConference(String value) {
        this.xInvitedConference = value;
    }

    /**
     * Obtient la valeur de la propriété xProceedings.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXProceedings() {
        return xProceedings;
    }

    /**
     * Définit la valeur de la propriété xProceedings.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXProceedings(String value) {
        this.xProceedings = value;
    }

    /**
     * Obtient la valeur de la propriété xScientificPopularization.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXScientificPopularization() {
        return xScientificPopularization;
    }

    /**
     * Définit la valeur de la propriété xScientificPopularization.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXScientificPopularization(String value) {
        this.xScientificPopularization = value;
    }

}
