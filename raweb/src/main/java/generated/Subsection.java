//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2018.01.24 à 09:57:38 AM CET 
//


package generated;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element ref="{}bodyTitle"/>
 *           &lt;element ref="{}moreinfo"/>
 *           &lt;element ref="{}p"/>
 *           &lt;element ref="{}subsection"/>
 *           &lt;element ref="{}participants"/>
 *           &lt;element ref="{}simplelist"/>
 *         &lt;/choice>
 *         &lt;choice minOccurs="0">
 *           &lt;element ref="{}object"/>
 *           &lt;element ref="{}sanspuceslist"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}NCName" />
 *       &lt;attribute name="level" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bodyTitleOrMoreinfoOrP",
    "object",
    "sanspuceslist"
})
@XmlRootElement(name = "subsection")
public class Subsection {

    @XmlElements({
        @XmlElement(name = "bodyTitle", type = BodyTitle.class),
        @XmlElement(name = "moreinfo", type = Moreinfo.class),
        @XmlElement(name = "p", type = P.class),
        @XmlElement(name = "subsection", type = Subsection.class),
        @XmlElement(name = "participants", type = Participants.class),
        @XmlElement(name = "simplelist", type = Simplelist.class)
    })
    protected List<java.lang.Object> bodyTitleOrMoreinfoOrP;
    protected generated.Object object;
    protected Sanspuceslist sanspuceslist;
    @XmlAttribute(name = "id", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String id;
    @XmlAttribute(name = "level", required = true)
    protected BigInteger level;

    /**
     * Gets the value of the bodyTitleOrMoreinfoOrP property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bodyTitleOrMoreinfoOrP property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBodyTitleOrMoreinfoOrP().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BodyTitle }
     * {@link Moreinfo }
     * {@link P }
     * {@link Subsection }
     * {@link Participants }
     * {@link Simplelist }
     * 
     * 
     */
    public List<java.lang.Object> getBodyTitleOrMoreinfoOrP() {
        if (bodyTitleOrMoreinfoOrP == null) {
            bodyTitleOrMoreinfoOrP = new ArrayList<java.lang.Object>();
        }
        return this.bodyTitleOrMoreinfoOrP;
    }

    /**
     * Obtient la valeur de la propriété object.
     * 
     * @return
     *     possible object is
     *     {@link generated.Object }
     *     
     */
    public generated.Object getObject() {
        return object;
    }

    /**
     * Définit la valeur de la propriété object.
     * 
     * @param value
     *     allowed object is
     *     {@link generated.Object }
     *     
     */
    public void setObject(generated.Object value) {
        this.object = value;
    }

    /**
     * Obtient la valeur de la propriété sanspuceslist.
     * 
     * @return
     *     possible object is
     *     {@link Sanspuceslist }
     *     
     */
    public Sanspuceslist getSanspuceslist() {
        return sanspuceslist;
    }

    /**
     * Définit la valeur de la propriété sanspuceslist.
     * 
     * @param value
     *     allowed object is
     *     {@link Sanspuceslist }
     *     
     */
    public void setSanspuceslist(Sanspuceslist value) {
        this.sanspuceslist = value;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété level.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLevel() {
        return level;
    }

    /**
     * Définit la valeur de la propriété level.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLevel(BigInteger value) {
        this.level = value;
    }

}
