//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2018.01.24 à 09:57:38 AM CET 
//


package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}shortname"/>
 *         &lt;element ref="{}projectName"/>
 *         &lt;element ref="{}theme-de-recherche"/>
 *         &lt;element ref="{}domaine-de-recherche"/>
 *         &lt;element ref="{}urlTeam"/>
 *         &lt;element ref="{}header_dates_team"/>
 *         &lt;element ref="{}LeTypeProjet"/>
 *         &lt;element ref="{}keywordsSdN"/>
 *         &lt;element ref="{}keywordsSecteurs"/>
 *         &lt;element ref="{}UR"/>
 *         &lt;element ref="{}moreinfo"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}NCName" />
 *       &lt;attribute name="isproject" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "shortname",
    "projectName",
    "themeDeRecherche",
    "domaineDeRecherche",
    "urlTeam",
    "headerDatesTeam",
    "leTypeProjet",
    "keywordsSdN",
    "keywordsSecteurs",
    "ur",
    "moreinfo"
})
@XmlRootElement(name = "identification")
public class Identification {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String shortname;
    @XmlElement(required = true)
    protected String projectName;
    @XmlElement(name = "theme-de-recherche", required = true)
    protected String themeDeRecherche;
    @XmlElement(name = "domaine-de-recherche", required = true)
    protected String domaineDeRecherche;
    @XmlElement(required = true)
    @XmlSchemaType(name = "anyURI")
    protected String urlTeam;
    @XmlElement(name = "header_dates_team", required = true)
    protected String headerDatesTeam;
    @XmlElement(name = "LeTypeProjet", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String leTypeProjet;
    @XmlElement(required = true)
    protected KeywordsSdN keywordsSdN;
    @XmlElement(required = true)
    protected KeywordsSecteurs keywordsSecteurs;
    @XmlElement(name = "UR", required = true)
    protected UR ur;
    @XmlElement(required = true)
    protected Moreinfo moreinfo;
    @XmlAttribute(name = "id", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String id;
    @XmlAttribute(name = "isproject", required = true)
    protected boolean isproject;

    /**
     * Obtient la valeur de la propriété shortname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortname() {
        return shortname;
    }

    /**
     * Définit la valeur de la propriété shortname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortname(String value) {
        this.shortname = value;
    }

    /**
     * Obtient la valeur de la propriété projectName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * Définit la valeur de la propriété projectName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProjectName(String value) {
        this.projectName = value;
    }

    /**
     * Obtient la valeur de la propriété themeDeRecherche.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThemeDeRecherche() {
        return themeDeRecherche;
    }

    /**
     * Définit la valeur de la propriété themeDeRecherche.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThemeDeRecherche(String value) {
        this.themeDeRecherche = value;
    }

    /**
     * Obtient la valeur de la propriété domaineDeRecherche.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomaineDeRecherche() {
        return domaineDeRecherche;
    }

    /**
     * Définit la valeur de la propriété domaineDeRecherche.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomaineDeRecherche(String value) {
        this.domaineDeRecherche = value;
    }

    /**
     * Obtient la valeur de la propriété urlTeam.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlTeam() {
        return urlTeam;
    }

    /**
     * Définit la valeur de la propriété urlTeam.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlTeam(String value) {
        this.urlTeam = value;
    }

    /**
     * Obtient la valeur de la propriété headerDatesTeam.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeaderDatesTeam() {
        return headerDatesTeam;
    }

    /**
     * Définit la valeur de la propriété headerDatesTeam.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeaderDatesTeam(String value) {
        this.headerDatesTeam = value;
    }

    /**
     * Obtient la valeur de la propriété leTypeProjet.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeTypeProjet() {
        return leTypeProjet;
    }

    /**
     * Définit la valeur de la propriété leTypeProjet.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeTypeProjet(String value) {
        this.leTypeProjet = value;
    }

    /**
     * Obtient la valeur de la propriété keywordsSdN.
     * 
     * @return
     *     possible object is
     *     {@link KeywordsSdN }
     *     
     */
    public KeywordsSdN getKeywordsSdN() {
        return keywordsSdN;
    }

    /**
     * Définit la valeur de la propriété keywordsSdN.
     * 
     * @param value
     *     allowed object is
     *     {@link KeywordsSdN }
     *     
     */
    public void setKeywordsSdN(KeywordsSdN value) {
        this.keywordsSdN = value;
    }

    /**
     * Obtient la valeur de la propriété keywordsSecteurs.
     * 
     * @return
     *     possible object is
     *     {@link KeywordsSecteurs }
     *     
     */
    public KeywordsSecteurs getKeywordsSecteurs() {
        return keywordsSecteurs;
    }

    /**
     * Définit la valeur de la propriété keywordsSecteurs.
     * 
     * @param value
     *     allowed object is
     *     {@link KeywordsSecteurs }
     *     
     */
    public void setKeywordsSecteurs(KeywordsSecteurs value) {
        this.keywordsSecteurs = value;
    }

    /**
     * Obtient la valeur de la propriété ur.
     * 
     * @return
     *     possible object is
     *     {@link UR }
     *     
     */
    public UR getUR() {
        return ur;
    }

    /**
     * Définit la valeur de la propriété ur.
     * 
     * @param value
     *     allowed object is
     *     {@link UR }
     *     
     */
    public void setUR(UR value) {
        this.ur = value;
    }

    /**
     * Obtient la valeur de la propriété moreinfo.
     * 
     * @return
     *     possible object is
     *     {@link Moreinfo }
     *     
     */
    public Moreinfo getMoreinfo() {
        return moreinfo;
    }

    /**
     * Définit la valeur de la propriété moreinfo.
     * 
     * @param value
     *     allowed object is
     *     {@link Moreinfo }
     *     
     */
    public void setMoreinfo(Moreinfo value) {
        this.moreinfo = value;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété isproject.
     * 
     */
    public boolean isIsproject() {
        return isproject;
    }

    /**
     * Définit la valeur de la propriété isproject.
     * 
     */
    public void setIsproject(boolean value) {
        this.isproject = value;
    }

}
