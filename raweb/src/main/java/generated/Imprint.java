//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2018.01.24 à 09:57:38 AM CET 
//


package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice maxOccurs="unbounded">
 *           &lt;element ref="{}biblScope"/>
 *           &lt;element ref="{}dateStruct"/>
 *           &lt;element ref="{}publisher"/>
 *         &lt;/choice>
 *         &lt;element ref="{}ref" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "biblScopeOrDateStructOrPublisher",
    "ref"
})
@XmlRootElement(name = "imprint")
public class Imprint {

    @XmlElements({
        @XmlElement(name = "biblScope", type = BiblScope.class),
        @XmlElement(name = "dateStruct", type = DateStruct.class),
        @XmlElement(name = "publisher", type = Publisher.class)
    })
    protected List<java.lang.Object> biblScopeOrDateStructOrPublisher;
    protected Ref ref;

    /**
     * Gets the value of the biblScopeOrDateStructOrPublisher property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the biblScopeOrDateStructOrPublisher property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBiblScopeOrDateStructOrPublisher().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BiblScope }
     * {@link DateStruct }
     * {@link Publisher }
     * 
     * 
     */
    public List<java.lang.Object> getBiblScopeOrDateStructOrPublisher() {
        if (biblScopeOrDateStructOrPublisher == null) {
            biblScopeOrDateStructOrPublisher = new ArrayList<java.lang.Object>();
        }
        return this.biblScopeOrDateStructOrPublisher;
    }

    /**
     * Obtient la valeur de la propriété ref.
     * 
     * @return
     *     possible object is
     *     {@link Ref }
     *     
     */
    public Ref getRef() {
        return ref;
    }

    /**
     * Définit la valeur de la propriété ref.
     * 
     * @param value
     *     allowed object is
     *     {@link Ref }
     *     
     */
    public void setRef(Ref value) {
        this.ref = value;
    }

}
