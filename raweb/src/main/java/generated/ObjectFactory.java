//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2018.01.24 à 09:57:38 AM CET 
//


package generated;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Tt_QNAME = new QName("", "tt");
    private final static QName _Loc_QNAME = new QName("", "loc");
    private final static QName _B_QNAME = new QName("", "b");
    private final static QName _Firstname_QNAME = new QName("", "firstname");
    private final static QName _HeaderDatesTeam_QNAME = new QName("", "header_dates_team");
    private final static QName _Year_QNAME = new QName("", "year");
    private final static QName _Initial_QNAME = new QName("", "initial");
    private final static QName _Num_QNAME = new QName("", "num");
    private final static QName _CategoryPro_QNAME = new QName("", "categoryPro");
    private final static QName _ThemeDeRecherche_QNAME = new QName("", "theme-de-recherche");
    private final static QName _I_QNAME = new QName("", "i");
    private final static QName _ResearchCentre_QNAME = new QName("", "research-centre");
    private final static QName _Shortname_QNAME = new QName("", "shortname");
    private final static QName _ForeName_QNAME = new QName("", "foreName");
    private final static QName _DomaineDeRecherche_QNAME = new QName("", "domaine-de-recherche");
    private final static QName _Lastname_QNAME = new QName("", "lastname");
    private final static QName _Month_QNAME = new QName("", "month");
    private final static QName _Surname_QNAME = new QName("", "surname");
    private final static QName _Hdr_QNAME = new QName("", "hdr");
    private final static QName _UrlTeam_QNAME = new QName("", "urlTeam");
    private final static QName _Term_QNAME = new QName("", "term");
    private final static QName _ProjectName_QNAME = new QName("", "projectName");
    private final static QName _LeTypeProjet_QNAME = new QName("", "LeTypeProjet");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Biblio }
     * 
     */
    public Biblio createBiblio() {
        return new Biblio();
    }

    /**
     * Create an instance of {@link BiblStruct }
     * 
     */
    public BiblStruct createBiblStruct() {
        return new BiblStruct();
    }

    /**
     * Create an instance of {@link Identifiant }
     * 
     */
    public Identifiant createIdentifiant() {
        return new Identifiant();
    }

    /**
     * Create an instance of {@link Analytic }
     * 
     */
    public Analytic createAnalytic() {
        return new Analytic();
    }

    /**
     * Create an instance of {@link Title }
     * 
     */
    public Title createTitle() {
        return new Title();
    }

    /**
     * Create an instance of {@link Formula }
     * 
     */
    public Formula createFormula() {
        return new Formula();
    }

    /**
     * Create an instance of {@link Author }
     * 
     */
    public Author createAuthor() {
        return new Author();
    }

    /**
     * Create an instance of {@link PersName }
     * 
     */
    public PersName createPersName() {
        return new PersName();
    }

    /**
     * Create an instance of {@link Monogr }
     * 
     */
    public Monogr createMonogr() {
        return new Monogr();
    }

    /**
     * Create an instance of {@link Idno }
     * 
     */
    public Idno createIdno() {
        return new Idno();
    }

    /**
     * Create an instance of {@link Editor }
     * 
     */
    public Editor createEditor() {
        return new Editor();
    }

    /**
     * Create an instance of {@link Imprint }
     * 
     */
    public Imprint createImprint() {
        return new Imprint();
    }

    /**
     * Create an instance of {@link BiblScope }
     * 
     */
    public BiblScope createBiblScope() {
        return new BiblScope();
    }

    /**
     * Create an instance of {@link DateStruct }
     * 
     */
    public DateStruct createDateStruct() {
        return new DateStruct();
    }

    /**
     * Create an instance of {@link Publisher }
     * 
     */
    public Publisher createPublisher() {
        return new Publisher();
    }

    /**
     * Create an instance of {@link OrgName }
     * 
     */
    public OrgName createOrgName() {
        return new OrgName();
    }

    /**
     * Create an instance of {@link Ref }
     * 
     */
    public Ref createRef() {
        return new Ref();
    }

    /**
     * Create an instance of {@link Allowbreak }
     * 
     */
    public Allowbreak createAllowbreak() {
        return new Allowbreak();
    }

    /**
     * Create an instance of {@link Meeting }
     * 
     */
    public Meeting createMeeting() {
        return new Meeting();
    }

    /**
     * Create an instance of {@link Abbr }
     * 
     */
    public Abbr createAbbr() {
        return new Abbr();
    }

    /**
     * Create an instance of {@link Note }
     * 
     */
    public Note createNote() {
        return new Note();
    }

    /**
     * Create an instance of {@link Caption }
     * 
     */
    public Caption createCaption() {
        return new Caption();
    }

    /**
     * Create an instance of {@link Presentation }
     * 
     */
    public Presentation createPresentation() {
        return new Presentation();
    }

    /**
     * Create an instance of {@link BodyTitle }
     * 
     */
    public BodyTitle createBodyTitle() {
        return new BodyTitle();
    }

    /**
     * Create an instance of {@link Subsection }
     * 
     */
    public Subsection createSubsection() {
        return new Subsection();
    }

    /**
     * Create an instance of {@link Moreinfo }
     * 
     */
    public Moreinfo createMoreinfo() {
        return new Moreinfo();
    }

    /**
     * Create an instance of {@link P }
     * 
     */
    public P createP() {
        return new P();
    }

    /**
     * Create an instance of {@link Footnote }
     * 
     */
    public Footnote createFootnote() {
        return new Footnote();
    }

    /**
     * Create an instance of {@link Span }
     * 
     */
    public Span createSpan() {
        return new Span();
    }

    /**
     * Create an instance of {@link Participants }
     * 
     */
    public Participants createParticipants() {
        return new Participants();
    }

    /**
     * Create an instance of {@link Person }
     * 
     */
    public Person createPerson() {
        return new Person();
    }

    /**
     * Create an instance of {@link Simplelist }
     * 
     */
    public Simplelist createSimplelist() {
        return new Simplelist();
    }

    /**
     * Create an instance of {@link Li }
     * 
     */
    public Li createLi() {
        return new Li();
    }

    /**
     * Create an instance of {@link Object }
     * 
     */
    public Object createObject() {
        return new Object();
    }

    /**
     * Create an instance of {@link Table }
     * 
     */
    public Table createTable() {
        return new Table();
    }

    /**
     * Create an instance of {@link Tr }
     * 
     */
    public Tr createTr() {
        return new Tr();
    }

    /**
     * Create an instance of {@link Td }
     * 
     */
    public Td createTd() {
        return new Td();
    }

    /**
     * Create an instance of {@link Ressource }
     * 
     */
    public Ressource createRessource() {
        return new Ressource();
    }

    /**
     * Create an instance of {@link Sanspuceslist }
     * 
     */
    public Sanspuceslist createSanspuceslist() {
        return new Sanspuceslist();
    }

    /**
     * Create an instance of {@link Identification }
     * 
     */
    public Identification createIdentification() {
        return new Identification();
    }

    /**
     * Create an instance of {@link KeywordsSdN }
     * 
     */
    public KeywordsSdN createKeywordsSdN() {
        return new KeywordsSdN();
    }

    /**
     * Create an instance of {@link KeywordsSecteurs }
     * 
     */
    public KeywordsSecteurs createKeywordsSecteurs() {
        return new KeywordsSecteurs();
    }

    /**
     * Create an instance of {@link UR }
     * 
     */
    public UR createUR() {
        return new UR();
    }

    /**
     * Create an instance of {@link Contrats }
     * 
     */
    public Contrats createContrats() {
        return new Contrats();
    }

    /**
     * Create an instance of {@link Fondements }
     * 
     */
    public Fondements createFondements() {
        return new Fondements();
    }

    /**
     * Create an instance of {@link Raweb }
     * 
     */
    public Raweb createRaweb() {
        return new Raweb();
    }

    /**
     * Create an instance of {@link Team }
     * 
     */
    public Team createTeam() {
        return new Team();
    }

    /**
     * Create an instance of {@link Highlights }
     * 
     */
    public Highlights createHighlights() {
        return new Highlights();
    }

    /**
     * Create an instance of {@link Logiciels }
     * 
     */
    public Logiciels createLogiciels() {
        return new Logiciels();
    }

    /**
     * Create an instance of {@link Resultats }
     * 
     */
    public Resultats createResultats() {
        return new Resultats();
    }

    /**
     * Create an instance of {@link Diffusion }
     * 
     */
    public Diffusion createDiffusion() {
        return new Diffusion();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tt")
    public JAXBElement<String> createTt(String value) {
        return new JAXBElement<String>(_Tt_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "loc")
    public JAXBElement<String> createLoc(String value) {
        return new JAXBElement<String>(_Loc_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "b")
    public JAXBElement<String> createB(String value) {
        return new JAXBElement<String>(_B_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "firstname")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createFirstname(String value) {
        return new JAXBElement<String>(_Firstname_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "header_dates_team")
    public JAXBElement<String> createHeaderDatesTeam(String value) {
        return new JAXBElement<String>(_HeaderDatesTeam_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "year")
    public JAXBElement<BigInteger> createYear(BigInteger value) {
        return new JAXBElement<BigInteger>(_Year_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "initial")
    public JAXBElement<String> createInitial(String value) {
        return new JAXBElement<String>(_Initial_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "num")
    public JAXBElement<BigInteger> createNum(BigInteger value) {
        return new JAXBElement<BigInteger>(_Num_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "categoryPro")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createCategoryPro(String value) {
        return new JAXBElement<String>(_CategoryPro_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "theme-de-recherche")
    public JAXBElement<String> createThemeDeRecherche(String value) {
        return new JAXBElement<String>(_ThemeDeRecherche_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "i")
    public JAXBElement<String> createI(String value) {
        return new JAXBElement<String>(_I_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "research-centre")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createResearchCentre(String value) {
        return new JAXBElement<String>(_ResearchCentre_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "shortname")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createShortname(String value) {
        return new JAXBElement<String>(_Shortname_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "foreName")
    public JAXBElement<String> createForeName(String value) {
        return new JAXBElement<String>(_ForeName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "domaine-de-recherche")
    public JAXBElement<String> createDomaineDeRecherche(String value) {
        return new JAXBElement<String>(_DomaineDeRecherche_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "lastname")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createLastname(String value) {
        return new JAXBElement<String>(_Lastname_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "month")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createMonth(String value) {
        return new JAXBElement<String>(_Month_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "surname")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createSurname(String value) {
        return new JAXBElement<String>(_Surname_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "hdr")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createHdr(String value) {
        return new JAXBElement<String>(_Hdr_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "urlTeam")
    public JAXBElement<String> createUrlTeam(String value) {
        return new JAXBElement<String>(_UrlTeam_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "term")
    public JAXBElement<String> createTerm(String value) {
        return new JAXBElement<String>(_Term_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "projectName")
    public JAXBElement<String> createProjectName(String value) {
        return new JAXBElement<String>(_ProjectName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LeTypeProjet")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createLeTypeProjet(String value) {
        return new JAXBElement<String>(_LeTypeProjet_QNAME, String.class, null, value);
    }

}
