//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2018.01.24 à 09:57:38 AM CET 
//


package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}identification"/>
 *         &lt;element ref="{}team"/>
 *         &lt;element ref="{}presentation"/>
 *         &lt;element ref="{}fondements"/>
 *         &lt;element ref="{}highlights"/>
 *         &lt;element ref="{}logiciels"/>
 *         &lt;element ref="{}resultats"/>
 *         &lt;element ref="{}contrats"/>
 *         &lt;element ref="{}diffusion"/>
 *         &lt;element ref="{}biblio"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "identification",
    "team",
    "presentation",
    "fondements",
    "highlights",
    "logiciels",
    "resultats",
    "contrats",
    "diffusion",
    "biblio"
})
@XmlRootElement(name = "raweb")
public class Raweb {

    @XmlElement(required = true)
    protected Identification identification;
    @XmlElement(required = true)
    protected Team team;
    @XmlElement(required = true)
    protected Presentation presentation;
    @XmlElement(required = true)
    protected Fondements fondements;
    @XmlElement(required = true)
    protected Highlights highlights;
    @XmlElement(required = true)
    protected Logiciels logiciels;
    @XmlElement(required = true)
    protected Resultats resultats;
    @XmlElement(required = true)
    protected Contrats contrats;
    @XmlElement(required = true)
    protected Diffusion diffusion;
    @XmlElement(required = true)
    protected Biblio biblio;

    /**
     * Obtient la valeur de la propriété identification.
     * 
     * @return
     *     possible object is
     *     {@link Identification }
     *     
     */
    public Identification getIdentification() {
        return identification;
    }

    /**
     * Définit la valeur de la propriété identification.
     * 
     * @param value
     *     allowed object is
     *     {@link Identification }
     *     
     */
    public void setIdentification(Identification value) {
        this.identification = value;
    }

    /**
     * Obtient la valeur de la propriété team.
     * 
     * @return
     *     possible object is
     *     {@link Team }
     *     
     */
    public Team getTeam() {
        return team;
    }

    /**
     * Définit la valeur de la propriété team.
     * 
     * @param value
     *     allowed object is
     *     {@link Team }
     *     
     */
    public void setTeam(Team value) {
        this.team = value;
    }

    /**
     * Obtient la valeur de la propriété presentation.
     * 
     * @return
     *     possible object is
     *     {@link Presentation }
     *     
     */
    public Presentation getPresentation() {
        return presentation;
    }

    /**
     * Définit la valeur de la propriété presentation.
     * 
     * @param value
     *     allowed object is
     *     {@link Presentation }
     *     
     */
    public void setPresentation(Presentation value) {
        this.presentation = value;
    }

    /**
     * Obtient la valeur de la propriété fondements.
     * 
     * @return
     *     possible object is
     *     {@link Fondements }
     *     
     */
    public Fondements getFondements() {
        return fondements;
    }

    /**
     * Définit la valeur de la propriété fondements.
     * 
     * @param value
     *     allowed object is
     *     {@link Fondements }
     *     
     */
    public void setFondements(Fondements value) {
        this.fondements = value;
    }

    /**
     * Obtient la valeur de la propriété highlights.
     * 
     * @return
     *     possible object is
     *     {@link Highlights }
     *     
     */
    public Highlights getHighlights() {
        return highlights;
    }

    /**
     * Définit la valeur de la propriété highlights.
     * 
     * @param value
     *     allowed object is
     *     {@link Highlights }
     *     
     */
    public void setHighlights(Highlights value) {
        this.highlights = value;
    }

    /**
     * Obtient la valeur de la propriété logiciels.
     * 
     * @return
     *     possible object is
     *     {@link Logiciels }
     *     
     */
    public Logiciels getLogiciels() {
        return logiciels;
    }

    /**
     * Définit la valeur de la propriété logiciels.
     * 
     * @param value
     *     allowed object is
     *     {@link Logiciels }
     *     
     */
    public void setLogiciels(Logiciels value) {
        this.logiciels = value;
    }

    /**
     * Obtient la valeur de la propriété resultats.
     * 
     * @return
     *     possible object is
     *     {@link Resultats }
     *     
     */
    public Resultats getResultats() {
        return resultats;
    }

    /**
     * Définit la valeur de la propriété resultats.
     * 
     * @param value
     *     allowed object is
     *     {@link Resultats }
     *     
     */
    public void setResultats(Resultats value) {
        this.resultats = value;
    }

    /**
     * Obtient la valeur de la propriété contrats.
     * 
     * @return
     *     possible object is
     *     {@link Contrats }
     *     
     */
    public Contrats getContrats() {
        return contrats;
    }

    /**
     * Définit la valeur de la propriété contrats.
     * 
     * @param value
     *     allowed object is
     *     {@link Contrats }
     *     
     */
    public void setContrats(Contrats value) {
        this.contrats = value;
    }

    /**
     * Obtient la valeur de la propriété diffusion.
     * 
     * @return
     *     possible object is
     *     {@link Diffusion }
     *     
     */
    public Diffusion getDiffusion() {
        return diffusion;
    }

    /**
     * Définit la valeur de la propriété diffusion.
     * 
     * @param value
     *     allowed object is
     *     {@link Diffusion }
     *     
     */
    public void setDiffusion(Diffusion value) {
        this.diffusion = value;
    }

    /**
     * Obtient la valeur de la propriété biblio.
     * 
     * @return
     *     possible object is
     *     {@link Biblio }
     *     
     */
    public Biblio getBiblio() {
        return biblio;
    }

    /**
     * Définit la valeur de la propriété biblio.
     * 
     * @param value
     *     allowed object is
     *     {@link Biblio }
     *     
     */
    public void setBiblio(Biblio value) {
        this.biblio = value;
    }

}
