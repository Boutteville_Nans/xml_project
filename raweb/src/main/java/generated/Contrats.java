//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2018.01.24 à 09:57:38 AM CET 
//


package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}bodyTitle"/>
 *         &lt;element ref="{}subsection"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}NCName" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bodyTitle",
    "subsection"
})
@XmlRootElement(name = "contrats")
public class Contrats {

    @XmlElement(required = true)
    protected BodyTitle bodyTitle;
    @XmlElement(required = true)
    protected Subsection subsection;
    @XmlAttribute(name = "id", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String id;

    /**
     * Obtient la valeur de la propriété bodyTitle.
     * 
     * @return
     *     possible object is
     *     {@link BodyTitle }
     *     
     */
    public BodyTitle getBodyTitle() {
        return bodyTitle;
    }

    /**
     * Définit la valeur de la propriété bodyTitle.
     * 
     * @param value
     *     allowed object is
     *     {@link BodyTitle }
     *     
     */
    public void setBodyTitle(BodyTitle value) {
        this.bodyTitle = value;
    }

    /**
     * Obtient la valeur de la propriété subsection.
     * 
     * @return
     *     possible object is
     *     {@link Subsection }
     *     
     */
    public Subsection getSubsection() {
        return subsection;
    }

    /**
     * Définit la valeur de la propriété subsection.
     * 
     * @param value
     *     allowed object is
     *     {@link Subsection }
     *     
     */
    public void setSubsection(Subsection value) {
        this.subsection = value;
    }

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
