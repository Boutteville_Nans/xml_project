//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2018.01.24 à 09:57:38 AM CET 
//


package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element ref="{}b"/>
 *         &lt;element ref="{}formula"/>
 *         &lt;element ref="{}ref"/>
 *         &lt;element ref="{}tt"/>
 *         &lt;element ref="{}footnote"/>
 *         &lt;element ref="{}i"/>
 *         &lt;element ref="{}span"/>
 *       &lt;/choice>
 *       &lt;attribute name="noindent" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="spacebefore" type="{http://www.w3.org/2001/XMLSchema}NMTOKEN" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "content"
})
@XmlRootElement(name = "p")
public class P {

    @XmlElementRefs({
        @XmlElementRef(name = "b", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "formula", type = Formula.class, required = false),
        @XmlElementRef(name = "footnote", type = Footnote.class, required = false),
        @XmlElementRef(name = "i", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ref", type = Ref.class, required = false),
        @XmlElementRef(name = "span", type = Span.class, required = false),
        @XmlElementRef(name = "tt", type = JAXBElement.class, required = false)
    })
    @XmlMixed
    protected List<java.lang.Object> content;
    @XmlAttribute(name = "noindent")
    protected Boolean noindent;
    @XmlAttribute(name = "spacebefore")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NMTOKEN")
    protected String spacebefore;

    /**
     * Gets the value of the content property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link Formula }
     * {@link Footnote }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link Ref }
     * {@link Span }
     * {@link String }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * 
     */
    public List<java.lang.Object> getContent() {
        if (content == null) {
            content = new ArrayList<java.lang.Object>();
        }
        return this.content;
    }

    /**
     * Obtient la valeur de la propriété noindent.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNoindent() {
        return noindent;
    }

    /**
     * Définit la valeur de la propriété noindent.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoindent(Boolean value) {
        this.noindent = value;
    }

    /**
     * Obtient la valeur de la propriété spacebefore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpacebefore() {
        return spacebefore;
    }

    /**
     * Définit la valeur de la propriété spacebefore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpacebefore(String value) {
        this.spacebefore = value;
    }

}
