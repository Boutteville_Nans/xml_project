//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2018.01.24 à 09:57:38 AM CET 
//


package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}firstname"/>
 *         &lt;element ref="{}lastname"/>
 *         &lt;sequence minOccurs="0">
 *           &lt;element ref="{}categoryPro"/>
 *           &lt;element ref="{}research-centre"/>
 *           &lt;element ref="{}moreinfo"/>
 *         &lt;/sequence>
 *         &lt;element ref="{}hdr" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="key" use="required" type="{http://www.w3.org/2001/XMLSchema}NCName" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "firstname",
    "lastname",
    "categoryPro",
    "researchCentre",
    "moreinfo",
    "hdr"
})
@XmlRootElement(name = "person")
public class Person {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String firstname;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String lastname;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String categoryPro;
    @XmlElement(name = "research-centre")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String researchCentre;
    protected Moreinfo moreinfo;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String hdr;
    @XmlAttribute(name = "key", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String key;

    /**
     * Obtient la valeur de la propriété firstname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Définit la valeur de la propriété firstname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstname(String value) {
        this.firstname = value;
    }

    /**
     * Obtient la valeur de la propriété lastname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Définit la valeur de la propriété lastname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastname(String value) {
        this.lastname = value;
    }

    /**
     * Obtient la valeur de la propriété categoryPro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryPro() {
        return categoryPro;
    }

    /**
     * Définit la valeur de la propriété categoryPro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryPro(String value) {
        this.categoryPro = value;
    }

    /**
     * Obtient la valeur de la propriété researchCentre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResearchCentre() {
        return researchCentre;
    }

    /**
     * Définit la valeur de la propriété researchCentre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResearchCentre(String value) {
        this.researchCentre = value;
    }

    /**
     * Obtient la valeur de la propriété moreinfo.
     * 
     * @return
     *     possible object is
     *     {@link Moreinfo }
     *     
     */
    public Moreinfo getMoreinfo() {
        return moreinfo;
    }

    /**
     * Définit la valeur de la propriété moreinfo.
     * 
     * @param value
     *     allowed object is
     *     {@link Moreinfo }
     *     
     */
    public void setMoreinfo(Moreinfo value) {
        this.moreinfo = value;
    }

    /**
     * Obtient la valeur de la propriété hdr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHdr() {
        return hdr;
    }

    /**
     * Définit la valeur de la propriété hdr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHdr(String value) {
        this.hdr = value;
    }

    /**
     * Obtient la valeur de la propriété key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Définit la valeur de la propriété key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

}
