package jaxb;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class AdapterBoolean extends XmlAdapter<String,Boolean> {
    @Override
    public Boolean unmarshal(String v) throws Exception {
        boolean t = Boolean.parseBoolean(v);
        return t;
    }

    @Override
    public String marshal(Boolean v) throws Exception {
        return v.toString();
    }
}
