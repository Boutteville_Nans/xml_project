import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.basex.api.client.ClientSession;
import org.basex.core.cmd.Add;
import org.basex.core.cmd.XQuery;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public abstract class RawebUtil {

	public static ClientSession VerifySessionDB(ClientSession session) throws IOException, SAXException, ParserConfigurationException{
		String dbExists = session.execute(new XQuery("db:exists('LocalDB')"));

		if(!dbExists.equals("true")) {
			session.execute("CREATE DB LocalDB");
		}else {
			session.execute("OPEN LocalDB");
		}
		
		String bastriExists = session.execute(new XQuery("db:exists('LocalDB','bastri.xml')"));
		if(!bastriExists.equals("true")) {
			session.execute(new XQuery("db:add('LocalDB','http://www-sop.inria.fr/members/Philippe.Poulard/projet/2017/bastri.xml','bastri.xml' )"));
		}
		
		String bastriCrisExists = session.execute(new XQuery("db:exists('LocalDB','bastriCris.xml')"));
		if(!bastriCrisExists.equals("true")) {
			session.execute(new XQuery("db:add('LocalDB','http://www-sop.inria.fr/members/Philippe.Poulard/projet/2017/bastriCris.xml','bastriCris.xml')"));
		}
		String xsdBastriExists = session.execute(new XQuery("db:exists('LocalDB','bastri.xsd')"));
		if(!xsdBastriExists.equals("true")) {
			session.execute(new XQuery("db:add('LocalDB','http://www-sop.inria.fr/members/Philippe.Poulard/projet/2017/bastri.xsd','bastri.xsd' )"));
		}
		
		String xsdBastriCrisExists = session.execute(new XQuery("db:exists('LocalDB','bastriCris.xsd')"));
		if(!xsdBastriCrisExists.equals("true")) {
			session.execute(new XQuery("db:add('LocalDB','http://www-sop.inria.fr/members/Philippe.Poulard/projet/2017/bastriCris.xsd','bastriCris.xsd')"));
		}
		
		String rawebExists = session.execute(new XQuery("db:exists('Raweb')"));
		if(!rawebExists.equals("true")) {
			session.execute("CREATE DB Raweb");
			
			InputStream input = new URL("http://www-sop.inria.fr/members/Philippe.Poulard/projet/2017/Raweb.zip").openStream();
			ZipInputStream zis = new ZipInputStream(input);
	        ZipEntry entry;
	        int n;
	        byte[] buf = new byte[2048];
	        int count = 0;
	        while ((entry = zis.getNextEntry()) != null)
	        {
	        	if(!entry.getName().startsWith("__")) {
	        		while (zis.available() > 0) {
	                //zis.read();
	                
		                System.out.println("entry: " + entry.getName() + ", " + entry.getSize());
		                FileOutputStream fos = new FileOutputStream("src/main/resources/tmp/tmpXML.xml");
		                while ((n = zis.read(buf, 0, 2048)) > -1) {
		                	fos.write(buf, 0, n);
		                }
		                fos.close();
	               
	        		} 
	        		session.execute(new XQuery("db:add('Raweb', 'src/main/resources/tmp/tmpXML.xml','"+entry.getName()+"')"));
	            }else zis.read();
	            
	        }
		}else {
			session.execute("OPEN Raweb");
		}
		
		return session;
	}
	
	public static ClientSession VerifySessionCache(ClientSession session) throws IOException, ParseException, SAXException, ParserConfigurationException{
		String timestampResources = session.execute(new XQuery("db:info('LocalDB')//resourceproperties/timestamp/text()"));
		timestampResources =  timestampResources.replace("T"," ");
		timestampResources = timestampResources.replace("Z","");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
	    Date parsedDate = dateFormat.parse(timestampResources);
	    
	    Timestamp timestamp = new Timestamp(parsedDate.getTime());
	    Timestamp now = new Timestamp(System.currentTimeMillis());

	    if(now.getTime() - timestamp.getTime() > 86400000) {
	    	session.execute(new XQuery("db:delete('LocalDB','bastri.xml')"));
	    	session.execute(new XQuery("db:delete('LocalDB','bastriCris.xml')"));
	    	session.execute(new XQuery("db:delete('LocalDB','bastri.xsd')"));
	    	session.execute(new XQuery("db:delete('LocalDB','bastriCris.xsd')"));
	    	
	    	session =  VerifySessionDB(session);
	    }
	    
	    String timestampRaweb = session.execute(new XQuery("db:info('Raweb')//resourceproperties/timestamp/text()"));
		timestampResources =  timestampResources.replace("T"," ");
		timestampResources = timestampResources.replace("Z","");
		
		SimpleDateFormat dateFormatRaweb = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
	    Date parsedDateRaweb = dateFormat.parse(timestampResources);
	    
	    Timestamp timestamp2 = new Timestamp(parsedDate.getTime());
	    Timestamp nowRaweb = new Timestamp(System.currentTimeMillis());

	    if(nowRaweb.getTime() - timestamp2.getTime() > 86400000) {
	    	session.execute("DROP DB Raweb");
	    	
	    	session =  VerifySessionDB(session);
	    }
	    
	    return session;
	}
	
	public static Document getXMLFromUrl(String url) throws MalformedURLException, IOException, SAXException, ParserConfigurationException {
		
		InputStream input = new URL("http://www-sop.inria.fr/members/Philippe.Poulard/projet/2017/bastri.xml").openStream();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		Document doc = null;
		
		DocumentBuilder builder = factory.newDocumentBuilder();
		doc = builder.parse(input);
		
		input.close();
		return doc;
	}

}
