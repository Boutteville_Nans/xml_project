import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graph-page',
  templateUrl: './graph-page.component.html',
  styleUrls: ['./graph-page.component.css']
})
export class GraphPageComponent implements OnInit {

  items:Array<{name:string,count:number,color:string}>=[
      {name:'Orange',count:50,color:'orange'},
      {name:'Apple',count:25,color:'red'},
      {name:'Pear',count:15,color:'green'}
    ];
    centerText={name:"Total",value:this.items.map(x=>x.count).reduce((x,y)=>x+y)};
  title = 'app works!';
  
  constructor() { }

  ngOnInit() {
  }

}
