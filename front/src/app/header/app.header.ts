import { Component } from '@angular/core';

@Component ({
  selector: 'app-head',
  template: `
    <div class="head">
      <ul>
        <li id="title">Projet XML-Web</li>
        <li><a routerLink="/" routerLinkActive="active">Home</a></li>
        <li><a routerLink="/map" routerLinkActive="active">Map</a></li>
        <li><a routerLink="/centresthemes" routerLinkActive="active">Centres & Thèmes</a></li>
        <li><a routerLink="/graphs" routerLinkActive="active">Graphiques</a></li>
      </ul>
    </div>
  <div class="menu">  </div>
  `,
  styleUrls: ['app.header.css'],
})

export class Header {
}
