import { Component, OnInit, OnChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { RequestService } from './../services/app.service';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {

  loading = true;
  observableObjects: Observable<Object[]>
  errorMessage: String;
  objects;
  cris:Array<{titre, themes}> = [];


   constructor(private RequestService: RequestService) {
   }

   getNbPersonnes(data): number{
     let count = 0;
     for (let i = 0 ; i<data.length ; i++){
       count = count + parseInt(data[i].nbPersonne);
     }
     return count;
   }

   whenReceived(objects): void {
     let tmp;
     let tms;
     for( let i = 0 ; i<this.objects.length ; i++){
       let tms = [];
       for (let j = 0; j < this.objects[i].themes.length; j++){
         if(this.getNbPersonnes(this.objects[i].themes[j].equipes) != 0){
           tms.push({nom : this.objects[i].themes[j].nom, nbPersonne : this.getNbPersonnes(this.objects[i].themes[j].equipes), color : this.getRandomColor()});
         }

       }
       tmp = {titre : this.objects[i].libelle, nbPersonne : this.objects[i].nbPersonne, themes : tms };
       this.cris.push(tmp);
     }
     this.loading=false;
   }

   ngOnInit(): void {

        this.observableObjects = this.RequestService.getlistTeamWithObservable();
        this.observableObjects.subscribe(
            objects => {this.objects = objects; this.whenReceived(this.objects)},
            error =>  this.errorMessage = <any>error);
   }


    getPerimeter(radius:number):number{
      return Math.PI*2*radius;
    }

    getColor(items):string{
      return items.color;
    }

    getOffset(radius:number, index:number, items, cri):number{
      var percent=0;
      for(var i=0;i<index;i++){
        percent+=((cri.themes[i].nbPersonne)/cri.nbPersonne);
      }
      var perimeter = Math.PI*2*radius;
      return perimeter*percent;
    }

    getRandomColor() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }
}
