import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamShowerComponent } from './team-shower.component';

describe('TeamShowerComponent', () => {
  let component: TeamShowerComponent;
  let fixture: ComponentFixture<TeamShowerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamShowerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamShowerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
