import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { AgmCoreModule } from '@agm/core';
import { RouterModule, Routes } from '@angular/router';

import { RequestService } from './services/app.service';
import { MyMap } from './map/app.map';
import { Header } from './header/app.header';
import { HomeComponent } from './home/home.component';
import {MatCardModule} from '@angular/material/card';
import { ListTeamComponent } from './list-team/list-team.component';
import { TeamShowerComponent } from './team-shower/team-shower.component';
import { GraphPageComponent } from './graph-page/graph-page.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'map', component: MyMap},
  {path: 'centresthemes', component: ListTeamComponent},
  {path: 'graphs', component: GraphPageComponent}
];

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    HttpModule,
    FormsModule,
    MatCardModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC8tzSY6zzbAnfwntNx7HIEkQgtl839aso'
    }),
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [
    RequestService
  ],
  declarations: [ Header, AppComponent, MyMap, HomeComponent, ListTeamComponent, TeamShowerComponent, GraphPageComponent, PieChartComponent],
  bootstrap: [ AppComponent ]
})
export class AppModule {}
