import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { RequestService } from './../services/app.service';
@Component ({
   selector: 'my-map',
   styleUrls: ['app.map.css'],
   template: `
   <div class="outer">
   <div *ngIf="loading">
     Chargement des données, cette opération peut prendre du temps...
   </div>
     <div class="inner">
       <agm-map [latitude]="lat" [longitude]="lng" [zoom]="zoom" >
        <div *ngFor="let object of objects">
          <agm-marker
            [latitude]="convertString(object.Adressegeographique.lattitude)"
            [longitude]="convertString(object.Adressegeographique.longitude)"
          >
              <agm-info-window>
                <h3>{{object.libelle}}</h3>
                <p>
                  <b>Adresse :</b> {{object.Adressegeographique.adresse}}, {{object.Adressegeographique.codePostal}}, {{object.Adressegeographique.ville}} <br/>
                  <b>Nombre d'équipes actives :</b> {{object.nbEquipeActive}} <br/>
                  <b>Nombre d'équipes fermées :</b> {{object.nbEquipeInActive}} <br/>
                  <b>Nombre de personnes :</b> {{object.nbPersonne}} <br/>
                  <b>Nombre de responsables :</b> {{object.nbResponsable}} <br/>
                </p>
              </agm-info-window>
          </agm-marker>
        </div>
       </agm-map>
     </div>
   </div>`,
})

export class MyMap implements OnInit{
  lat: number = 46.5;
  lng: number = 2.3522;
  zoom: number = 5;
  observableObjects: Observable<Object[]>
  objects: Object[];
  errorMessage: String;
  loading = true;
  convertString(value){
    return parseFloat(value);
  }

  constructor(private RequestService: RequestService) { }

  ngOnInit(): void {
       this.observableObjects = this.RequestService.getlistResearchCenterWithObservable();
       this.observableObjects.subscribe(
           objects => {this.objects = objects ; this.loading=false},
           error =>  this.errorMessage = <any>error);
  }
}
