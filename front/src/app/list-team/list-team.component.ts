import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { RequestService } from './../services/app.service';
@Component({
  selector: 'app-list-team',
  template: `
  <div *ngIf="!loading">
    <div *ngFor="let cri of objects; let i = index">
      <mat-card>
        <h2>{{cri.libelle}}</h2>
        <app-team-shower>
          <em>voir les thèmes</em>
          <div *ngFor="let theme of cri.themes; let j = index">
            <mat-card>
              <h3>{{theme.nom}}</h3>
              <app-team-shower>
                <em>voir les équipes</em>
                <div  *ngFor="let equipe of theme.equipes" >
                  <mat-card>
                    <p>
                      <b>ID : </b>{{equipe.id}}<br/>
                      <b>Libelle : </b>{{equipe.libelle}}<br/>
                      <b>Domaine : </b>{{equipe.Domaine}}<br/>
                      <b>Nombre de membre : </b>{{equipe.nbPersonne}}<br/>
                    </p>
                  </mat-card>
                </div>
              </app-team-shower>
            </mat-card>
          </div>
        </app-team-shower>
      </mat-card>
    </div>
  </div>
  <div *ngIf="loading">
    Chargement des données, cette opération peut prendre du temps...
  </div>
  `,
  styleUrls: ['./list-team.component.css']
})
export class ListTeamComponent implements OnInit {
  observableObjects: Observable<Object[]>
  objects: Object[];
  errorMessage: String;
  loading = true;
  constructor(private RequestService: RequestService) { }

  ngOnInit(): void {
       this.observableObjects = this.RequestService.getlistTeamWithObservable();
       this.observableObjects.subscribe(
           objects => {this.objects = objects ; this.loading=false},
           error =>  this.errorMessage = <any>error);
  }

}
