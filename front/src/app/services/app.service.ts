//http://httpbin.org/ip
/* * * ./app/comments/services/comment.service.ts * * */
// Imports
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class RequestService {
    url = "http://localhost:8080/";
    constructor(private http:Http) { }

    getlistResearchCenterWithObservable(): Observable<Object[]> {
        return this.http.get(this.url + "listResearchCenter")
	        .map(this.extractData)
	        .catch(this.handleErrorObservable);
    }

    getlistTeamWithObservable(): Observable<Object[]> {
        return this.http.get(this.url + "listTeam")
	        .map(this.extractData)
	        .catch(this.handleErrorObservable);
    }

    private extractData(res: Response) {
	  let body = res.json();
    console.log(body);
        return body;
    }
    private handleErrorObservable (error: Response | any) {
    	console.error(error.message || error);
    	return Observable.throw(error.message || error);
    }
}
