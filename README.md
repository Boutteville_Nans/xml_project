# README #

### Lancement du projet ###

#### Back-end ####
pré-requis : Maven.

Démarche de lancement : se placer dans le dossier raweb et lancer la command "mvn jetty:run".


#### Front-end ####
pré-requis : Node > 6.9.x, npm > 3.x.x, angular.
	Pour installer angular, une fois node et npm installer, lancer la commande "npm install -g @angular/cli".

Démarche de lancement : se placer dans le dossier front puis lancer la commande "npm install", puis "ng serve".